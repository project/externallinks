/**
 * @file
 * External Link.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.externalLink = {
    attach: function (context) {
      $('a',context).once('externalLink').on('click', function () {
        var link = $(this).attr('href');
        var exit_title = drupalSettings.disclaimerinfo.disclaimer_title;
        var exit_description = drupalSettings.disclaimerinfo.description;
        var exit_button = drupalSettings.disclaimerinfo.button_label_exit;
        var stay_in_website_button = drupalSettings.disclaimerinfo.button_label_stay;

        if (isExternal(link, drupalSettings.disclaimerinfo.exceptions)) {
          $('<div>' + exit_description + '</div>').dialog({
            create: function (event, ui) {
              $('body').addClass('overlay--active');
              $(event.target).parent().css('position', 'fixed');
            },
            beforeClose: function (event, ui) {
              $('body').removeClass('overlay--active');
            },
            title: exit_title,
            modal: true,
            dialogClass: 'exit__site--popup',
            draggable: false,
            overlay: {
              backgroundColor: '#000',
              opacity: 0.8
            },
            buttons: [
              {
                text: stay_in_website_button,
                class: 'btn--gray',
                click: function () {
                  $(this).dialog('close').remove();
                }
              },
              {
                text: exit_button,
                class: 'btn--gray',
                click: function () {
                  $(this).dialog('close').remove();
                  window.open(link);
                }
              }
            ]
          });

          return false;
        }

      });
    }
  };
}(jQuery, Drupal, drupalSettings));
