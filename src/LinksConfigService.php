<?php

namespace Drupal\externallink;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * LinksConfigService service.
 */
class LinksConfigService {
  use StringTranslationTrait;



  protected $config;


  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;



  /**
   * Constructs.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   */
  public function __construct(
                              ConfigFactoryInterface $config
                            ) {
    $this->config = $config->get('externallink.default');
    $this->configFactory = $config;

  }

  /**
   * Gets the information to construct the UI for disclaimer.
   *
   * @return array
   *   an array of variables with the cookie informations.
   */
  public function getDisclaimerInfo() {
    $data = [];
    $data['variables']['disclaimer_title'] = !empty($this->config->get('disclaimer_title')) ? $this->config->get('disclaimer_title') : $this->t('DISCLAIMER_TITLE');
    $data['variables']['description'] = !empty($this->config->get('description')) ? $this->config->get('description') : $this->t('ADD_DESCRIPTION');
    $data['variables']['exceptions'] = !empty($this->config->get('exceptions')) ? $this->config->get('exceptions') : '';
    $data['variables']['button_label_exit'] = !empty($this->config->get('button_label_exit')) ? $this->config->get('button_label_exit') : $this->t('EXIT_LABEL');
    $data['variables']['button_label_stay'] = !empty($this->config->get('button_label_stay')) ? $this->config->get('button_label_stay') : $this->t('STAY_LABEL');

    return $data;
  }

}
