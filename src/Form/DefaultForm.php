<?php

namespace Drupal\externallink\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DefaultForm.
 */
class DefaultForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'externallink.default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('externallink.default');
    $form['disclaimer_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Disclaimer Title'),
      '#description' => $this->t('Popup Title'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('disclaimer_title'),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('description'),
    ];
    $form['exceptions'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exception Domain'),
      '#default_value' => $config->get('exceptions'),
    ];
    $form['button_label_exit'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Label ( Exit )'),
      '#description' => $this->t('Label for the button Exit'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('button_label_exit'),
    ];
    $form['button_label_stay'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Label ( Stay )'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('button_label_stay'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('externallink.default')
      ->set('disclaimer_title', $form_state->getValue('disclaimer_title'))
      ->set('description', $form_state->getValue('description'))
      ->set('exceptions', $form_state->getValue('exceptions'))
      ->set('button_label_exit', $form_state->getValue('button_label_exit'))
      ->set('button_label_stay', $form_state->getValue('button_label_stay'))
      ->save();
  }

}
